/** random module
 * @module random
 *
 * Generate pseudo-random numbers
 */

import * as seedrandom from 'seedrandom'


/** Initialize the random number generator
  * @function
  * @param seed
  */
export const seed = (seed) => {
    random = seedrandom.default(seed)
}

/**
 * 
 * @param {*} a start-point 
 * @param {*} b end-point
 */
export const randint = (a, b) => {
    const min = Math.min(a, b)
    const max = Math.max(a, b)
    return Math.floor(random() * (max - min)) + min  
}

/**
 * Returns a k length list of unique elements from population
 * 
 * @param {list} population 
 * @param {int} k sample size 
 */
export const sample = (population, k) => {
  const my_pop = []
  const choices = []
  while (my_pop.length < k) {
    const elem = randint(0, k)
    if (choices.indexOf(elem) > -1) continue
    choices.push(elem)
    my_pop.push(population[elem])
  }
  return my_pop
}

export let random = Math.random


export const uniform = (a, b) => {
    const min = Math.min(a, b)
    const max = Math.max(a, b)
    return random() * (max - min) + min
}