/** Basic infrastructure for pyes6
  */

class NotImplemented {
    constructor(message) {
        this._message = message
    }

    get message() {return this._message}
}

import * as builtins from './builtins.js'
import * as time from './time.js'
import * as timeit from './timeit.js'

export {NotImplemented, builtins, time, timeit}
