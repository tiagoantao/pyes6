/** Timeit module. Based on Python timeit
 * @module timeit
 */

import {NotImplemented} from './index.js'

import {ValueError} from './builtins.js'

import {perf_counter} from './time.js'

const default_number = 1000000
const default_repeat = 3
const default_timer = perf_counter
const pass = () => {}

const timeit = function(stmt=pass, setup=pass,
    timer=default_timer, number=default_number, globals={}) {
    return new Timer(stmt, setup, timer, globals).timeit(number)
}

const repeat = function(stmt=pass, setup=pass,
    timer=default_timer, repeat=default_repeat, number=default_number,
    globals={}) {
    return new Timer(stmt, setup, timer, globals).timeit(repeat, number)
}

/** Class for timing execution speed of small code snippets
  *
  * A very poor implementation for now
  */
class Timer {
    constructor(stmt=pass, setup=pass, timer=default_timer, globals={}) {
        this._globals = globals
        this._timer = timer
        if (typeof setup == 'string') {
            throw NotImplemented('timeit.Timer: Statement as string not supported')
        }
        else if (typeof setup == 'function') {
            this._setup = setup
        }
        else {
            throw ValueError("setup is neither a string nor callable")
        }
        if (typeof stmt == 'string') {
            throw NotImplemented('timeit.Timer: Statement as string not supported')
        }
        else if (typeof stmt == 'function') {
            this._stmt = stmt
        }
        else {
            throw ValueError("stmt is neither a string nor callable")
        }
  }

  repeat(repeat=default_repeat, number=default_number) {
      const r = []
      for (let i=0; i<repeat; i++) {
          const t = this.timeit(number)
          r.push(t)
      }
      return r
  }

  timeit(number=default_number) {
      //TODO: cleanup the namespace
      //TODO: use setup
      const start = this._timer()
      for (let rep=0; rep<number; rep++) {
          this._stmt()

      }
      const end = this._timer()
      return end - start
  }
}

export {repeat, timeit, Timer}
