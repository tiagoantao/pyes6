/** Timeit module. Based on Python timeit
 * @module timeit
 */


/** Returns a time value in fractional seconds.
  *
  * Only usable to compare among calls, doesn't have a reference point
  * @function
  */
const perf_counter = function() {
  let d = new Date()
  return d.getTime() / 1000
}

export {perf_counter}
