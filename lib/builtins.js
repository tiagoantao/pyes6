/** Builtin module. Based on Python __builtins__
 * @module builtins
 *
 * Includes exceptions
 */


 class BaseException {
     constructor(message) {
         this._message = message
     }

     get message() {return this._message}
 }


class ValueError extends BaseException {

}

/** Absolute value of a number
  * @function
  * @param number
  */
const abs = Math.abs


const all = function(iterable) {
    const iterator = iterable[Symbol.iterator]()

    for (let elem of iterator) {
        if (!elem) return false
    }
    return true
}


const any = function(iterable) {
    const iterator = iterable[Symbol.iterator]()

    for (let elem of iterator) {
        if (elem) return true
    }
    return false
}


/** Returns a generator for a range
  *
  * Uses:
  *  range(stop)
  *  range(start, stop, [step])
  * @function
  */
const range = function*(...values) {
    let start = 0, stop = null, step = 1
    if (values.length == 1) {
        stop = values[0]
    }
    else if (values.length == 3) {
        start = values[0]
        stop = values[1]
        step = values[2]
    }
    else {
        // TODO: throw if other parameter lengths
    }

    for (let point = start; point != stop; point += step ) {
        yield point
    }
}


/* Makes an iterator that aggregates elements from each of the iterables.
 *
 */
const zip = function*(...iterables) {
    const iterators = []
    for (const iter of iterables) {
        iterators.push(iter[Symbol.iterator]())
    }
    let done = false
    for (;;) {
        const value = []
        for (let iter of iterators) {
            const my_next = iter.next()
            value.push(my_next.value)
            done = done || my_next.done
            if (done) break
        }
        if (done) break
        yield value // TODO: return tuple instead of list?
    }
}

export {abs, all, any, BaseException, range, ValueError, zip }
