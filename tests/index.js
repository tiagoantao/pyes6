/*eslint-env mocha, node*/
import * as py from '../lib/index.js'
var assert = require('assert')

describe('check imports', function() {
    it('imports', () => {
        assert.ok(py.builtins)
        assert.ok(py.builtins.abs)
        assert.ok(py.time)
        assert.ok(py.timeit)
    })
})
