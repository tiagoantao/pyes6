/*eslint-env mocha, node*/
import * as random from '../lib/random.js'
var assert = require('assert')

describe('Random module', function() {
    it('seed', () => {
        random.seed(1)
        assert(Math.abs(random.random() - 0.177) < 0.01)
    })
    it('sample', () => {
        let sample = random.sample([1,2], 2)
        assert.equal(sample.length, 2)
        assert.equal(sample[0] + sample[1], 3)
    })
})
