/*eslint-env mocha, node*/
import {all, any, range, zip} from '../lib/builtins.js'
var assert = require('assert')

describe('All & Any', function() {
    it('all', function() {
        assert.equal(true, all([true, 1, 2]))
        assert.equal(false, all([true, false, 2]))
        assert.equal(false, all([true, 0, 2]))
    })
    it('any', function() {
        assert.equal(true, any([true, 1, 2]))
        assert.equal(true, any([true, false, 2]))
        assert.equal(false, any([false, false, false]))
        assert.equal(false, any([false, 0, 0]))
    })
})

describe('range', function() {
    it('Simple range', function() {
        assert.deepEqual([0, 1, 2], [...range(3)])
    })
    it('Range with start, stop, step', function() {
        assert.deepEqual([1, 3, 5], [...range(1, 7, 2)])
    })
    it('Negative step', function() {
        assert.deepEqual([7, 5, 3], [...range(7, 1, -2)])
    })
})

describe('zip', function() {
    const l1 = [1, 2]
    const l2 = [4, 5]
    const l3 = [4, 5, 6]
    it('Simple zip', function() {
        assert.deepEqual([[1, 4], [2, 5]], [...zip(l1, l2)])
    })
    it('Different sized iterators', function() {
        assert.deepEqual([[1, 4], [2, 5]], [...zip(l1, l3)])
    })
})
