/*eslint-env mocha, node*/
import {perf_counter} from '../lib/time.js'
var assert = require('assert')

describe('perf_counter', function() {
    it('Simple Ordering', function(done) {
        let t1 = perf_counter()
        setTimeout(function() {
            let t2 = perf_counter()
            assert(t1 < t2 + 99)
            done()
        }, 100)
    })
})
