# Python flavored libraries for JavaScript (ES6)

[![Build Status](https://travis-ci.org/tiagoantao/pyes6.svg?branch=master)](https://travis-ci.org/tiagoantao/pyes6) [![Coverage Status](https://coveralls.io/repos/github/tiagoantao/pyes6/badge.svg?branch=master)](https://coveralls.io/github/tiagoantao/pyes6?branch=master) [![Code Climate](https://codeclimate.com/github/tiagoantao/pyes6/badges/gpa.svg)](https://codeclimate.com/github/tiagoantao/pyes6) [![bitHound Overall Score](https://www.bithound.io/github/tiagoantao/pyes6/badges/score.svg)](https://www.bithound.io/github/tiagoantao/pyes6)

[![dependencies Status](https://david-dm.org/tiagoantao/pyes6/status.svg)](https://david-dm.org/tiagoantao/pyes6) [![devDependencies Status](https://david-dm.org/tiagoantao/pyes6/dev-status.svg)](https://david-dm.org/tiagoantao/pyes6?type=dev)
